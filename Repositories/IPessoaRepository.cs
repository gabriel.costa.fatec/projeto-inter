public interface IPessoaRepository
{
    List<Pessoa> Read();
    void Create(Pessoa pessoa);
    Pessoa Read(int id);
    void Update(int id, Pessoa pessoa);
    void Delete(int id);

    List<Pessoa> Search(string pesquisa);
}