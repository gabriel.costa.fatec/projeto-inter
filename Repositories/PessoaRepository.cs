using Microsoft.Data.SqlClient;

public class PessoaRepository : Database, IPessoaRepository
{
    public void Create(Pessoa pessoa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Pessoas VALUES (@nome, @cpf, @telefone, @email, @endereco, @cod)";

        cmd.Parameters.AddWithValue("@nome", pessoa.Nome);
        cmd.Parameters.AddWithValue("@cpf", pessoa.CPF);
        cmd.Parameters.AddWithValue("@telefone", pessoa.Telefone);
        cmd.Parameters.AddWithValue("@email", pessoa.Email);
        cmd.Parameters.AddWithValue("@endereco", pessoa.Endereco);
        cmd.Parameters.AddWithValue("@cod", pessoa.CodIdentificador);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Pessoas WHERE idPessoa = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }

    public List<Pessoa> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Pessoas";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Pessoa> pessoas = new List<Pessoa>();

        while(reader.Read())
        {
            Pessoa p = new Pessoa();
            p.IdPessoa = reader.GetInt32(0);
            p.Nome = reader.GetString(1);
            p.CPF = reader.GetString(2);
            p.Telefone = reader.GetString(3);
            p.Email = reader.GetString(4);
            p.Endereco = reader.GetString(5);
            p.CodIdentificador = reader.GetInt32(6);


            pessoas.Add(p);
        }

        return pessoas;

    }

    public Pessoa Read(int id)
    {
        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Pessoas WHERE idPessoa = @id";

        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            Pessoa p = new Pessoa();
            p.IdPessoa = reader.GetInt32(0);
            p.Nome = reader.GetString(1);
            p.CPF = reader.GetString(2);
            p.Telefone = reader.GetString(3);
            p.Email = reader.GetString(4);
            p.Endereco = reader.GetString(5);
            p.CodIdentificador = reader.GetInt32(6);

            return p;
        }

        return null;

    }

    public List<Pessoa> Search(string pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Pessoas WHERE Nome like @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Pessoa> pessoas = new List<Pessoa>();

        while(reader.Read())
        {
            Pessoa p = new Pessoa();
            p.IdPessoa = reader.GetInt32(0);
            p.Nome = reader.GetString(1);
            p.Telefone = reader.GetString(3);
            p.Email = reader.GetString(4);
            p.Endereco = reader.GetString(5);
            p.CodIdentificador = reader.GetInt32(6);


            pessoas.Add(p);
        }

        return pessoas;

    }

    void IPessoaRepository.Update(int id, Pessoa pessoa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "UPDATE Pessoas SET nome = @nome, CPF = @cpf, telefone = @telefone, email = @email, endereco = @endereco, codIdentificador = @cod WHERE idPessoa = @id";
        cmd.Parameters.AddWithValue("@nome", pessoa.Nome);
        cmd.Parameters.AddWithValue("@cpf", pessoa.CPF);
        cmd.Parameters.AddWithValue("@telefone", pessoa.Telefone);
        cmd.Parameters.AddWithValue("@email", pessoa.Email);
        cmd.Parameters.AddWithValue("@endereco", pessoa.Endereco);
        cmd.Parameters.AddWithValue("@id", id);
        cmd.Parameters.AddWithValue("@COD", pessoa.CodIdentificador);

        cmd.ExecuteNonQuery();

    }
}