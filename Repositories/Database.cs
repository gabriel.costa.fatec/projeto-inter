using Microsoft.Data.SqlClient;

public abstract class Database : IDisposable
{
    protected SqlConnection conn;

    public Database()
    {
        string connectionString = "Data Source=DESKTOP-LAB0516\\MSSQLSERVER02; Initial Catalog=LojaTintas; Integrated Security=True; TrustServerCertificate=true";

        conn = new SqlConnection(connectionString);
        conn.Open();

        Console.WriteLine("Conexão aberta");
    }

    public void Dispose() // Fecha a conexão
    {
        conn.Close();
        Console.WriteLine("Conexão fechada");
    }
}
