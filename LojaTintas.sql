create database LojaTintas
go

use LojaTintas
go

SELECT * FROM Pessoas
go

create table Pessoas
(
	idPessoa				int				not null	primary key		identity,
	nome					varchar(50)		not null,
	CPF						varchar(14)		not null	unique,
	telefone				varchar(15)		not null,
	email					varchar(50)			null	unique,
	endereco				varchar(150)	not null,
	codIdentificador		int				not null	check (codIdentificador IN (1, 2)),
)
go

-- create table Funcionarios
-- (
-- 	pessoaId	int				not null	primary key		identity,
-- 	salario		decimal(10,2)	null,
-- 	beneficios	decimal(10,2)	null,
-- 	foreign key (pessoaId)		references Pessoas(idPessoa)
-- )
-- go

-- create table Entregadores
-- (
-- 	pessoaId	int				not null	primary key		identity,
-- 	CNH			int				not null	unique,
-- 	salario		decimal(10,2)	null,
-- 	foreign key (pessoaId)		references Pessoas(idPessoa)
-- )
-- go

-- create table Clientes
-- (
-- 	pessoaId	int				not null	primary key		identity,
-- 	bonus		decimal(10,2)	null,
-- 	foreign key (pessoaId)		references Pessoas(idPessoa)
-- )
-- go

create table Fornecedores
(
	idFornecedor	int			not null	primary key		identity,
	CNPJ			varchar(18)	not null	unique,
	nome			varchar(50)	not null,
	email			varchar(50)	not null	unique,
	telefone		varchar(15)	not null,
)
go

create table Produtos
(
	SKU				int				not null	primary key		identity,
	fornecedorId	int				not null,
	preco			decimal(10,2)	not null,
	UM				varchar(4)		not null,
	peso			decimal(10,2)	not null,
	nome			varchar(50)		not null,
	descricao		varchar(100)	not null,
	qtdEstoque		int				not null,
	foreign key	(fornecedorId)		references	Fornecedores(idFornecedor)
)
go

create table Pedidos
(
	idPedido		int				not null	primary key		identity,
	data			datetime		not null,
	clienteId		int				not null,
	funcionarioId	int				not null,
	valor			decimal(10,2)	not null,
	foreign key (clienteId)			references	Clientes(pessoaId),
	foreign key	(funcionarioId)		references	Funcionarios(pessoaId)
)
go

create table Listas_Produtos
(
	idLista			int				not null	primary key		identity,
	SKUproduto		int				not null,
	qtdPedido		int				not null,
	pedidoId		int				not null,
	foreign key (SKUproduto)		references Produtos(SKU),
	foreign key (pedidoId)			references	Pedidos(idPedido)
)
go

create table Movimentacoes
(
	idMov			int				not null	primary key		identity,
	data			datetime		not null,
	status			varchar(20)		not null,
	SKUproduto		int				not null,
	qtdEntrega		int				not null,
	numeroNF		int				not null,
	funcionarioId	int				not null,
	foreign key (SKUproduto)		references Produtos(SKU),
	foreign key	(funcionarioId)		references	Funcionarios(pessoaId)
)
go


-- create table Entregas
-- (
-- 	idEntrega		int				not null	primary key		identity,
-- 	entregadorId	int				not null,
-- 	data			varchar(10)		not null,
-- 	hora			varchar(5)		not null,
-- 	pedidoId		int				not null,
-- 	enderecoId		int				not null,
-- 	foreign key (entregadorId)		references	Entregadores(pessoaId),
-- 	foreign key (pedidoId)			references	Pedidos(idPedido),
-- 	foreign key (enderecoId)	references	Enderecos(idEndereco)
-- )
-- go