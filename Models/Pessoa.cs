public class Pessoa
{
    private int idPessoa;
    private string nome;
    private string cpf;
    private string telefone;
    private string email;
    private string endereco;
    private int codIdentificador;


    public int IdPessoa
    {
        get { return this.idPessoa; }
        set { this.idPessoa = value; }
    }
    public string Nome
    {
        get { return this.nome; }
        set { this.nome = value; }
    }
    public string CPF
    {
        get { return this.cpf; }
        set { this.cpf = value; }
    }
    public string Telefone
    {
        get { return this.telefone; }
        set { this.telefone = value; }
    }
    public string Email
    {
        get { return this.email; }
        set { this.email = value; }
    }

    public string Endereco
    {
        get { return this.endereco; }
        set { this.endereco = value; }
    }

    public int CodIdentificador
    {
        get { return this.codIdentificador; }
        set { this.codIdentificador = value; }
    }
}