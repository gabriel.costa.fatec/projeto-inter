var builder = WebApplication.CreateBuilder(args);

// add middlewares
builder.Services.AddControllersWithViews();
builder.Services.AddTransient<IPessoaRepository, PessoaRepository>();

var app = builder.Build();

// setup middleware
app.MapControllerRoute("default", "/{controller=Pessoa}/{action=Index}/{id?}");
// app.MapControllers(); // using [Route("")]

app.Run();
