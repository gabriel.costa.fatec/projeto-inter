using Microsoft.AspNetCore.Mvc;

public class PessoaController : Controller
{
    IPessoaRepository pessoaRepository;

    public PessoaController(IPessoaRepository pessoaRepository)
    {
        this.pessoaRepository = pessoaRepository;
    }
    
    public ActionResult Index()
    {
        return View(pessoaRepository.Read());
    }

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Pessoa model)
    {
        pessoaRepository.Create(model);
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {
        Pessoa p = pessoaRepository.Read(id);
        if(p != null)
            return View(p);

        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        pessoaRepository.Delete(id);
        return RedirectToAction("Index");
    }

    public ActionResult Update(int id)
    {
        Pessoa p = pessoaRepository.Read(id);
        if(p != null)
            return View(p);

        return NotFound();
    }

    [HttpPost]
    public ActionResult Update(int id, Pessoa model)
    {
        pessoaRepository.Update(id, model);
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", pessoaRepository.Search(pesquisa));
    }
}

